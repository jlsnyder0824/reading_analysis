import bs4
import requests
import logging
import numpy as np
import pandas as pd
import typing

def get_and_create_list(classname:str):
    """
    Get the full list of book titles
    """
    finding_list = html_table.find_all(class_=classname)
    return finding_list


def turn_list_into_df(listname:str, column_name:str):
    """
    Take the list and make it into a dataframe.
    """
    as_df = pd.DataFrame(listname)
    as_df.columns = [column_name]
    return as_df


def get_title_and_author(classname:str, column_name:str):
    """
    Append author info to title.
    """
    making_title_list = []
    for i in get_and_create_list(classname):
        new_item = i.find("a").contents[0].strip().replace("\n", "")
        making_title_list.append(new_item)
    return turn_list_into_df(making_title_list, column_name)


def get_date_read(classname:str, column_name:str):
    """
    Pull the date finished value. (Also used for the date started value.)
    """
    making_list = []
    for i in get_and_create_list(classname):
        try:
            new_item = i.contents[0]
        except:
            new_item = "Skipping"
        new_item = pd.to_datetime(new_item)
        making_list.append(new_item)
    return turn_list_into_df(making_list, column_name)


def get_number_of_pages_in_book(classname:str, column_name:str):
    """
    Get number of pages in the book according to goodreads.
    """
    making_list = []
    for i in get_and_create_list(classname):
        new_item = (
            i.find(class_="value")
            .contents[1]
            .get_text()
            .strip()
            .replace("\n", "")
            .replace("pp", "")
            .strip()
        )
        try:
            making_list.append(float(new_item))
        except:
            making_list.append(None)
    return turn_list_into_df(making_list, column_name)


def create_shelf(
    titleclassname:str,
    title_column_name:str,
    authorclassname:str,
    author_column_name:str,
    date_read_value:str,
    bookclassname:str,
    book_column_name:str,
    date_started_value:str,
):
    """
    Building the full shelf from extracted data (only).
    """
    ## Add title + author
    df = pd.concat(
        [
            get_title_and_author(titleclassname, title_column_name)[title_column_name],
            get_title_and_author(authorclassname, author_column_name)[
                author_column_name
            ],
        ],
        axis=1,
        join_axes=[
            get_title_and_author(titleclassname, title_column_name)[
                title_column_name
            ].index
        ],
    )
    ## Add date read
    df = pd.concat(
        [df, get_date_read(date_read_value, date_read_value)[date_read_value]],
        axis=1,
        join_axes=[df.index],
    )
    ## Add number of pages in book
    df = pd.concat(
        [
            df,
            get_number_of_pages_in_book(bookclassname, book_column_name)[
                book_column_name
            ],
        ],
        axis=1,
        join_axes=[df.index],
    )
    ## Add start dategit
    df = pd.concat(
        [
            df,
            get_date_read(date_started_value, date_started_value)[date_started_value],
        ],  ##date added
        axis=1,
        join_axes=[df.index],
    )
    return df

if __name__ == "__main__":
    """
    Build the shelf, do a small transformation, save to csv
    """
    logging.basicConfig(level=20)

    logging.info("Setting variables.. ")
    url = "https://www.goodreads.com/review/list/37809322-emilie-burke?order=d&shelf=read&sort=date_added"
    response = requests.get(url)  # commented out so I don't rerun accidentally
    html = response.text
    soup = bs4.BeautifulSoup(html, "html.parser")
    html_table = soup.find(id="booksBody")
    logging.info("All variables set..")

    logging.info("Building shelf..")
    data = create_shelf(
        "field title",
        "field_title",
        "field author",
        "field_author",
        "date_read_value",
        "field num_pages",
        "field_num_pages",
        "date_started_value",
    )
    logging.info("Shelf almost built..")
    data["days_to_read"] = (
        pd.to_timedelta(data["date_read_value"] - data["date_started_value"])
    ).dt.days
    data["avg_pages_per_day"] = data["field_num_pages"] / data["days_to_read"]
    logging.info("Shelf built..")

    logging.info("Writing to csv..")
    data.to_csv("data.csv", sep=",", encoding="utf-8")
    logging.info("Let there be data..")
